# Functional programming (FP)
Scala, Kotlin and JVM

### Functional programming
Functional programming (FP) is based on a simple premise with far-reaching implications: we
construct our programs using only pure functions — in other words, functions that have no side
effects.

What are side effects? A function has a side effect if it does something other than simply
return a result, for example:
1. Modifying a variable
2. Modifying a data structure in place
3. Setting a field on an object
4. Throwing an exception or halting with an error
5. Printing to the console or reading user input
6. Reading from or writing to a file
7. Drawing on the screen

### Pure functions
Functional programming based on pure-function. __Pure-function__ - is one that has no side effects.

So, pure attributes:
1. It's return value is the same for the same arguments. 
2. It's evaluation has no side effects (no mutation of local static variables, non-local variables, mutable reference arguments or I/O streams).

Example of pure function:
```
fun sum(a: Int, b: Int): Int {
    return a + b
}
```

Examples of non-pure functions:
```
fun nonPure(): Int {
    return x
}
```
This function is not pure because of return value variation with a non-local variable. `x` can be changed somehow and another call of `nonPure()` can return different result.
In this case we violate first rule.

```
fun nonPure(): Int {
    var x: Int = 0
    return ++x
}
```
In this case we violate second rule. Also, integer overflow can happens, therefore, this behavior is not idempotent.


### Higher-order function
__Higher-order function__ ([HOF](https://kotlinlang.org/docs/reference/lambdas.html#higher-order-functions)) are functions that take other functions as arguments and
may themselves return functions as their output.

The first new idea is this: functions are values. And just like values of other types—such as integers,
strings, and lists—functions can be assigned to variables, stored in data structures, and passed as
arguments to functions.

Good example - [fold](https://medium.com/@zaid.naom/exploring-folds-a-powerful-pattern-of-functional-programming-3036974205c8) function.

Fold function can have next signature:
```
fold(identity, operation, list)
```
Where identity - initial value. Operation - another function, which represents operation and list of input variables.

Example of implementation:
```
fun <T, R> fold(identity: R, operation: (acc: R, T) -> R, list: List<T>): R {
    var accumulatedValue = identity

    for (element in list) {
        accumulatedValue = operation(accumulatedValue, element)
    }

    return accumulatedValue
}
```

So, `fold` can be used in many ways. It's executes some operation of list of values and depends on initial value.

1. We can find sum of list:
```
fold(0, { first, second -> first + second }, listOf(1, 2, 3, 4)) // 10
```

2. Find max of list:
```
fold(0, { max, element -> if (element > max) element else max }, listOf(5, 0, 1, 3, 11, -2)) // 11
```

3. Multiply list on numbers:
```
fold(1, {first, second -> first * second}, listOf(1, 2, 3)) // 6
```

`Fold` function is really depends on initial value (identity value), here is __incorrect example__:
```
fold(-5, { min, element -> if (element < min) element else min }, listOf(2, -2, 10)) // -5
```

Also, kotlin and scala supports [fold function](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/fold.html) via collections on API level. The same situation can be detected for [reduce function](https://stackoverflow.com/questions/44429419/what-is-basic-difference-between-fold-and-reduce-in-kotlin-when-to-use-which).

### Currying
Currying is the technique of converting a function that takes multiple arguments into a sequence of functions that each takes a single argument.

<a href="https://ibb.co/MgypCj2"><img src="https://i.ibb.co/10YKmpv/image.png" alt="image" border="0"></a>

Or in other words:

```
∀ 𝑓: (A x B) → C
h(𝑓): A → B → C
```

Where `𝑓` - some function and `h(𝑓)` - currying result.

Example:

We have function with signature `def curringSumOfRange(mapper: Int => Int)(start: Int, end: Int): Int`.
Several params brackets meaning next return type `(Int => Int) => (Int, Int) => Int` or `(Int => Int) => ((Int, Int) => Int)`.

Usage:

```
 def curringSumOfRange(mapper: Int => Int)(start: Int, end: Int): Int = {
    @tailrec
    def loop(loopStart: Int, accumulator: Int): Int = {
      if (loopStart > end) accumulator
      else loop(loopStart + 1, accumulator + mapper(loopStart))
    }

    loop(start, 0)
}
```

If function is defined, now we can use it:

```
val qube = (x: Int) => x * x * x
curringSumOfRange(qube)(1, 4) // 1^3 + 2^3 + 3^3 + 4^3
```

### Lazy evaluation
Lazy evaluation delays the evaluation of an expression until its value is needed and which also avoids repeated evaluations.

Let's see some examples.

[Java Stream API](https://docs.oracle.com/javase/8/docs/api/java/util/stream/package-summary.html):
```
 val list = listOf(1, 2, 3, 4, 5, -6, 7, 8, 9, -10)
 list.stream()
     .map { abs(it) }
     .filter { it % 2 == 0 }
     .findFirst()
     .orElse(null)
```
>Intermediate operations return a new stream. They are always lazy; executing an intermediate operation such as filter() does not actually perform any filtering, but instead creates a new stream that, when traversed, contains the elements of the initial stream that match the given predicate. Traversal of the pipeline source does not begin until the terminal operation of the pipeline is executed.

Here we can detect next operations pipelines:
1. map value `1` to `Math.abs(1)`
2. filter predicate for `1 % 2 == 0`, predicate is false
3. map value `2` to `Math.abs(2)`
4. filter predicate for `2 % 2 == 0`, predicate is true, we can go ahead
5. find first value from stream and return it

So, here we don't iterate over all elements, then filter all elements and after that just evaluate first value. That's example of __lazy evaluation__.

[Kotlin Sequences API](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.sequences/) works in the same way:
```
val list = listOf(1, 2, 3, 4, 5, -6, 7, 8, 9, -10)
 list.asSequence()
      .map { abs(it) }
      .filter { it % 2 == 0 }
      .firstOrNull()
```

So, lazy evaluation force to evaluate new step each time:

<a href="https://ibb.co/qDtX9m7"><img src="https://i.ibb.co/RgxZPQY/image.png" alt="image" border="0"></a>

Example of not lazy evalueation:
```
val list = listOf(1, 2, -3, 4)
list.map { abs(it) }
    .filter { it % 2 == 0 }
    .firstOrNull()
```
Will be executed in next steps:
1. map value `1` to `Math.abs(1)`
2. map value `2` to `Math.abs(2)`
3. map value `-3` to `Math.abs(-3)`
4. map value `4` to `Math.abs(4)`
5. filter predicate for `1 % 2 == 0`, predicate is false
6. filter predicate for `2 % 2 == 0`, predicate is true
7. filter predicate for `3 % 2 == 0`, predicate is false
8. filter predicate for `4 % 2 == 0`, predicate is true
9. return first (`2`) filtered value

Final review picture:

<a href="https://ibb.co/Gs2GY0k"><img src="https://i.ibb.co/9wTBKN2/image.png" alt="image" border="0"></a>

#### Parallel execution
I want to specify one important thing, streams __can be executed in parallel__, but sequnces - __not__.

Difference in details between sequences and streams described [here](https://proandroiddev.com/java-streams-vs-kotlin-sequences-c9ae080abfdc) and [here](https://typealias.com/guides/when-to-use-sequences/).

### Terminal and non-terminal operations
The Stream interface has a selection of __terminal__ and __non-terminal__ [operations](http://tutorials.jenkov.com/java-functional-programming/streams.html#terminal-and-non-terminal-operations). 

1. The non-terminal stream operations of the Java Stream API are operations that transform or filter the elements in the stream. When you add a non-terminal operation to a stream, you get a new stream back as result. The new stream represents the stream of elements resulting from the original stream with the non-terminal operation applied. Here is an example of a non-terminal operation added to a stream - which results in a new stream:
```
val newStream = someStream.map { someOperation }
```
`map` call actually returns a new Stream instance representing the original stream with the map operation applied. In addition, `map` - pure function, which return new "state".

List of non-terminal stream operations:
* `filter()`
* `map()`
* `flatMap()`
* `distinct()`
* `limit()`
* `peek()`


2. The terminal operations of the Java Stream interface typicall return a single value. Once the terminal operation is invoked on a Stream, the iteration of the Stream and any of the chained streams will get started. Once the iteration is done, the result of the terminal operation is returned. 
```
val count: Long = stream
  .map { someOperation }
  .count();
```
It is the call to `count()` at the end of the example that is the terminal operation. Since `count()` returns a long, the Stream chain of non-terminal operations (the `map()` calls) is ended.

List of terminal stream operations:
* `anyMatch()`
* `allMatch()`
* `noneMatch()`
* `collect()`
* `count()`
* `findAny()`
* `findFirst()`
* `forEach()`
* `min()`
* `max()`
* `reduce()`
* `toArray()`

_Some additional [information](https://www.baeldung.com/java-stream-close) about closing streams._

### Recursion
In functional languages loops can be replaced by recursion. But yes, we know that recursion - is very expensive stack framed aspect of language, isn't it?

Let's look on next sample. It's fibonnaci recurent formula:

<a href="https://imgbb.com/"><img src="https://i.ibb.co/7KdFwLL/image.png" alt="image" border="0"></a>

And Kotlin implementation:
```
fun fibonacci(n: Int): Int {
    return when (n) {
        0 -> 0
        1 -> 1
        else -> fibonacci(n - 1) + fibonacci(n - 2)
    }
}
```

Main problem of such solution - a lot of dublication of stack frames, which consumes CPU (intermediate results aren’t being saved). For example, let's calculate 5-th number of fibonacci sequence:
```
fib(5) = fib(4) + fib(3) 
       = fib(3) + fib(2) + fib(2) + fib(1) 
       = fib(2) + fib(1) + fib(1) + fib(0) 
                + fib(1) + fib(0) + 1
       = fib(1) + fib(0) + 1 + 1 + 0 + 1 + 0 + 1 
       = 1 + 0 + 1 + 1 + 0 + 1 + 0 + 1
       = 5
```

But actually, how recursion works fast in functional languages? Answer - [tail recursion](https://dev.to/rohit/demystifying-tail-call-optimization-5bf3)

### Tail recursion
__Tail recursion__ - the last statement in a function is recursive call. 
```
fun tailFibonacci(n: Int, first: Int = 0, second: Int = 1): Int {
    if (n == 0) {
        return first
    }
       
    if (n == 1) {
        return second
    }

    return tailFibonacci(n - 1, second, first + second)
}
```

Or more Kotlin style:
```
fun tailFibonacci(n: Int, first: Int = 0, second: Int = 1): Int =
    when (n) {
        0 -> first
        1 -> second
        else -> tailFibonacci(n - 1, second, first + second)
    }
```
Tail call optimization reduces the space complexity of recursion from O(n) to O(1). Our function would require constant memory for execution. It does so by eliminating the need for having a separate stack frame for every call.

>If a function is tail recursive, it's either making a simple recursive call or returning the value from that call. No computation is performed on the returned value. Thus, there is no real need to preserve the stack frame for that call. We won't need any of the local data once the tail recursive call is made: we don't have any more statements or computations left. We can simply modify the state of the frame as per the call arguments and jump back to the first statement of the function. No need to push a new stack frame! We can do this over and over again with just one stack frame!

For [JVM optimization](https://kousenit.org/2019/11/26/fibonacci-in-kotlin/), Kotlin have special [tailrec keyword](https://kotlinlang.org/docs/reference/functions.html#tail-recursive-functions).
```
tailrec fun tailFibonacci(n: Int, first: Int = 0, second: Int = 1): Int =
    when (n) {
        0 -> first
        1 -> second
        else -> tailFibonacci(n - 1, second, first + second)
    }
```

### Evaluation strategy
In a programming language, an evaluation strategy is a set of rules for evaluating expressions. There are two basic types of evaluion:
* Strict evaluation
* Non-strict evaluation

__Strict evaluation__, in which the arguments are evaluated before the function is applied. 
Examples of programming languages that use strict evaluation strategies are Java, Scheme, JavaScript etc

__Non-strict evaluation__, which will defer the evaluation of the arguments until they are actually required/used in the function body. 
Haskell is probably the most popular functional programming language that uses non-strict evaluation.

__Scala__ supports both of the types, which categorized via:
* __call-by-value__
* __call-by-name__

__Call by value__ (CBV)
Call by value is a strict evaluation strategy which is the most commonly used by programming languages. In call by value, the expression is evaluated and bound to the corresponding parameter before the function body is evaluated. It is also the default evaluation strategy in Scala.

```
def first(a: Int, b: Int) = a
```

If we will invoke `first` function and pass as second param some expression, then it will evaluated before function invocation.

__Call by name__ (CBN)
Call by name is a non-strict evaluation strategy which will defer the evaluation of the expression until the program needs it. 
To make a parameter call-by-name just add `=>` before the type, as shown bellow:

```
def first(a: Int, b: => Int) = a
```

For example, let's try to pass as second param infinite loop:

```
def loop(): Int = loop()
...
first(1, loop) // Or first(1, loop())
```

In case of __CBV__, execution will stuck because of jeager evaluation. In case of __CBN__, fuction will returns `1`.

### Lambda (λ) calculus
The λ-calculus and combinatory logic are two systems of logic which can also serve as abstract programming languages. 
They both aim to describe some very general properties of programs that can modify other programs, in an abstract setting not cluttered by details. 
In some ways they are rivals, in others they support each other.

There are two main principles of λ-calculus:
1. Abstraction
2. Application

1) Let's imagine, that we have expression `M` with which contains some computition `x`:  `M ≡ M[x]`. 
In that case, we can define __abstraction__ `λx. M` (or `𝑓: x → M[x]`). Each `x` mapped with specific `M[x]`.

Alonzo Church (creator of λ-calculus) introduced new way of defining function in mathematical point of view.
As example, we have next function notation: `𝑓(x) = x - y`. Such notation also can be introduced as `𝑓: x → x - y`.
And now it can be replaced via `𝑓 = λx . x − y`.


2) Let's imagine that we have function `𝑓: x → x + 5`. Or in terms of λ-calculus `λx. x + 5`.
When we want to compute result of function, then we must apply argument:

```
(λx. x + 5) (2) = 2 + 5 = 7
```

It's important to mention, that `(λx. x + 5)` - anonymus function.

Example of abstraction and application usage:

```
𝑓(0) = 0 − y
𝑓(1) = 1 - y
```

With λ-calculus it might be replaced as:
```
(λx . x − y)(0) = 0 − y
(λx . x − y)(1) = 1 − y
```

### λ-terms
A valid λ-calculus expression is called a "lambda term" or "λ-term".
The following three rules give an inductive definition that can be applied to build all syntactically valid lambda terms: 
1. Variable `x` is itself a valid λ-term 
2. If `t` is a λ-term, and `x` is a variable, then `λx. t` is a lambda term (called an __abstraction__)
3. If `t` and `s` are λ-terms, then `t s` is a lambda term (called an __application__)

Nothing else is a lambda term. Thus a lambda term is valid __if and only if it can be obtained by repeated application of these three rules__.

__Examples__:

1) Let's imagine, that we have some sequence of expressions `v_1, v_2, v_3, . . . `.
And in that case `(λ v_1 .(v_2 v_3))` is a __valid__ λ-term.

2) If `x`, `y`, `z` are any distinct variables, the following are λ-terms:

<a href="https://ibb.co/1fspjWD"><img src="https://i.ibb.co/TBKnd3V/image.png" alt="image" border="0"></a>

### β-reduction


### Category theory
Category theory is a branch of mathematics that studies the properties of relations between mathematical objects that do not depend on the internal structure of the objects. 

There are two basic units: __functions__ and __sets (types)__.

* types - set of possible values
* functions (pure) - mappings between types (sets)

As example, we have next types:

```
trait Rock
trait Sand
trait Glass
```

Each specific type ([trait](https://docs.scala-lang.org/tour/traits.html#inner-main) in terms of Scala)

Also, we have pure functions, which transforms one type to another:

```
def crush(rock: Rock): Sand
def heat(sand: Sand): Glass
```

And we have mapping between types:

```
val rock: Rock = ...
val glass: Glass = heat(crush(rock)) // Composition of types
```

Such associations between functions and types can be presented as oriented graph. Objects/types/sets represented as nodes, functions represented as vertexes:

<a href="https://ibb.co/gy4khGw"><img src="https://i.ibb.co/fMDjLbt/image.png" alt="image" border="0"></a>

Connection between types called __morphism__. Such structure called __category__.

It's really important to mention, that each object inside category have identity morphism:
```
∀ α ∃ id(α) : α → α
```

And that means next equality: `id_α ∘ α =  α ∘ id_α = α`

In other words: For each type α inside category exists special identity __morphism__ id_α, which referrences to this type α.

### Category properties
Each category have next basic properties:
* [Composition](https://math24.net/composition-functions.html) (the application of one function to the result of another)
* Associativity of compositions (property of composition)

__Composition example__:

We can apply `x` after `g`:
```
b = g(1)
(x ∘ g) (1) = x(g(1)) = x(b)
```

If we return to example with graph, there are vertex with __morphism__ `g ∘ f`, which not specified directly. 
You can see, that composition of morphisms adds new transitions between nodes. Such property called [transitivity](https://en.wikipedia.org/wiki/Transitive_relation).

The composition of functions is associative. If `h: A → B`, `g: B → C` and `f: C → D`, then:
```
f ∘ (g ∘ h) = (f ∘ g) ∘ h
```

It's called __associativity axiom__.

<a href="https://ibb.co/tX0zcg8"><img src="https://i.ibb.co/9txh26s/image.png" alt="image" border="0"></a>

Also, there is identity axiom:

```
∀ 𝑓: a → b:
id_b ∘ 𝑓 = 𝑓 = 𝑓 ∘ id_a
```

<a href="https://ibb.co/FJ3R4qp"><img src="https://i.ibb.co/tcKVpPR/image.png" alt="image" border="0"></a><br /><a target='_blank' href='https://imgbb.com/'></a><br />

```
𝑓(a) = b
(𝑓 ∘ id_a)(a) = 𝑓(a) = b
(id_b ∘ 𝑓)(a) = id_b(b) = b
```

### Category constructions

1. __Singleton__

Such category contains only one type (object) with identity morphism. Such category also called as __trivial__ one.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/dL5zC4N/image.png" alt="image" border="0"></a>

2. __Isomorphism__

>The word isomorphism is derived from the Ancient Greek: ἴσος isos "equal", and μορφή morphe "form" or "shape". 

The interest in isomorphisms lies in the fact that two isomorphic objects have the same properties (excluding further information such as additional structure or names of objects.
Thus isomorphic structures cannot be distinguished from the point of view of structure only, and may be identified. 
In mathematical jargon, one says that two objects are the same up to an isomorphism.

Let `C` be a category. A morphism `𝑓 ∶ a → b` is an __isomorphism__ if there is an inverse morphism `g ∶ b → a` such that both ways to compose `𝑓` and `g` give the identity morphisms on the respective objects:

```
g ∘ f = id_a
f ∘ g = id_b
```

<a href="https://ibb.co/Lpj684y"><img src="https://i.ibb.co/2KVPyXr/image.png" alt="image" border="0"></a>

The strongest kind of morphism we can consider is an isomorphism. An isomorphism is the way we say two objects in a category are “the same.” We don’t usually care whether two objects are equal, but rather whether some construction is unique up to isomorphism (more on that when we talk of universal properties). The choices made in defining morphisms in a particular category allow us to strengthen or weaken this idea of “sameness.”

<a href="https://ibb.co/LtQKBdf"><img src="https://i.ibb.co/WyDC7v1/image.png" alt="image" border="0"></a>

3. __Monomorphism__


